﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T : BaseEntity
    {
        protected IEnumerable<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data;
        }

        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task<Guid> AddAsync(T entity)
        {
            Data = Data.Append(entity);
            return Task.FromResult(entity.Id);
        }

        public Task EraseAsync(Guid id)
        {
            var entity = Data.FirstOrDefault(x => x.Id == id);
            if (entity == null)
            {
                return Task.CompletedTask;
            }

            Data = Data.Where(x => x.Id != id);
            return Task.CompletedTask;
        }

        public Task<Guid> UpdateAsync(T entity)
        {
            var existingEntity = Data.FirstOrDefault(x => x.Id == entity.Id);
            if (existingEntity == null)
            {
                return Task.FromResult(entity.Id);
            }

            Data = Data.Select(x => x.Id == entity.Id ? entity : x);
            return Task.FromResult(entity.Id);
        }

        public Task<IEnumerable<T>> GetByListIdAsync(IEnumerable<Guid> ids)
        {
            var result = Data.Where(x => ids.Contains(x.Id));
            return Task.FromResult(result);
        }
    }
}