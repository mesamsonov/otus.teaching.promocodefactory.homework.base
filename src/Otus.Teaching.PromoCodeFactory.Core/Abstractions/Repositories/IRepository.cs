﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories
{
    public interface IRepository<T>
        where T : BaseEntity
    {
        /// <summary>
        /// Получить список сущностей
        /// </summary>
        /// <returns></returns>
        Task<IEnumerable<T>> GetAllAsync();

        /// <summary>
        /// Получить сущность по Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<T> GetByIdAsync(Guid id);


        /// <summary>
        /// Добавление сущности
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        Task<Guid> AddAsync(T entity);

        /// <summary>
        /// Удаление сущности
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task EraseAsync(Guid id);

        /// <summary>
        /// Обновление сотрудника
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        Task<Guid> UpdateAsync(T entity);

        /// <summary>
        /// Получение списка сущностей по списку Id
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        public Task<IEnumerable<T>> GetByListIdAsync(IEnumerable<Guid> ids);
    }
}