﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class EmployeeRequest
    {
        [Required(ErrorMessage = "First name is required")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Last name is required")]
        public string LastName { get; set; }
        
        [EmailAddress(ErrorMessage = "Email is required")]
        public string Email { get; set; }

        public IEnumerable<Guid> RoleIds{ get; set; }

        [Range(0, int.MaxValue, ErrorMessage = "Only positive number allowed")]
        public int AppliedPromocodesCount { get; set; }
    }
}